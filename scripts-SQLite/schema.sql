CREATE TABLE Medicament ( 
codeCIS TEXT, 
denomination TEXT,
formePharma TEXT, 
voieAdmin TEXT, 
statutAdminAMM TEXT, 
typeProcAMM TEXT, 
etatCommercialisation TEXT, 
dateAMM TEXT, 
statusBDM TEXT, 
numAutEurop TEXT, 
titulaire TEXT, 
surveillance TEXT, 
conditionPrescription TEXT, 
PRIMARY KEY (codeCIS));

CREATE TABLE Substance ( 
codeS TEXT, 
denomination TEXT,
PRIMARY KEY (codeS));

CREATE TABLE Presentation ( 
codeCIP7 TEXT, 
libellePres TEXT,
statutPres TEXT, 
etatCommercialisation TEXT, 
dateCommercialisation TEXT, 
codeCIP13 TEXT, 
agrementCollectivites TEXT, 
tauxRbt TEXT, 
prixEuro TEXT, 
texteRbt TEXT, 
codeCIS TEXT, 
PRIMARY KEY (codeCIP7),
FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS));

CREATE TABLE TypeGenerique ( 
codeTG INTEGER, 
libelleTG TEXT,
PRIMARY KEY (codeTG));

CREATE TABLE GroupeGenerique ( 
identifiant TEXT, 
numeroOrdre TEXT,
libelle TEXT,
codeTG TEXT,
codeCIS TEXT,
PRIMARY KEY (identifiant, numeroOrdre)FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS),FOREIGN KEY (codeTG) REFERENCES TypeGenerique(codeTG));

CREATE TABLE Dosage ( 
codeCIS TEXT, 
codeS TEXT,
designElemPharma TEXT,
dosage TEXT,
reference TEXT,
nature TEXT,
numSAFT TEXT,
PRIMARY KEY (codeCIS, codeS)FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS), 
FOREIGN KEY (codeS) REFERENCES Substance(codeS));

CREATE TABLE AvisHAS ( 
codeDossierHAS TEXT, 
motifEval TEXT,
dateAvis TEXT,
codeCIS TEXT,
asmrVSsmr TEXT,
valeurASMR TEXT,
libelleASMR TEXT,
valeurSMR TEXT,
libelleSMR TEXT,
lienAvisCT TEXT,
PRIMARY KEY (codeDossierHAS) FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS));

CREATE TABLE PresentationMedicament ( 
ncodeCIS TEXT, 
codeCIP7 TEXT,
etatCommercialisation TEXT, 
